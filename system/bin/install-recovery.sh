#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/platform/13500000.dwmmc0/by-name/recovery:25280784:8eaa6de43c82b132a27d43e2f7dd240042d0730a; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/platform/13500000.dwmmc0/by-name/boot:16843024:0e990baa23c4c988b2403416f23ee5cd7baa4471 EMMC:/dev/block/platform/13500000.dwmmc0/by-name/recovery 8eaa6de43c82b132a27d43e2f7dd240042d0730a 25280784 0e990baa23c4c988b2403416f23ee5cd7baa4471:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
